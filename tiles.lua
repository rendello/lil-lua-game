#!/usr/bin/lua5.3

tile = {
	wall = {
		img = love.graphics.newImage('Assets/8/wall.png'),
		passable = false
	},
	wall_II = {
		img = love.graphics.newImage('Assets/8/wall_II.png'),
		passable = false
	},
	floor = {
		img = love.graphics.newImage('Assets/8/floor.png'),
		passable = true
	},
	lava = {
		img = love.graphics.newImage('Assets/8/lava.png'),
		passable = true
	}
}

autotile = {
	[1] = 'Assets/8/wall.png',
	[2] = 1,
	[3] = 1,
	[4] = 'Assets/8/wall_II.png',
	[5] = 4,
	[6] = 4,
	[7] = 4,
	[8] = 1,
	[9] = 1,
	[10] = 1,
	[11] = 1,
	[12] = 4.
	[13] = 4,
	[14] = 4,
	[15] = 4
},
