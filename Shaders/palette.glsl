
extern vec4 color1;
extern vec4 color2;
extern vec4 color3;
extern vec4 color4;

extern int start_light;
extern int end_light;


vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords ){
	vec4 pixel = Texel(texture, texture_coords); //This is the current pixel color
	if(screen_coords.x > start_light && screen_coords.x < end_light) {
		return pixel * color;
	} else {
		return pixel;
	}
}


