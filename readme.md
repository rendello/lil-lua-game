# Rendello's Lua Roguelike

I've been wanting to make my own game from the ground up for years, so here it
is! I'm trying to go light on the libraries and heavy on the implementation,
reading up on theory when I need to :D

The game is made in Lua using the Löve (aka Love2d) engine, and is inspired by
[this clip](https://old.reddit.com/r/roguelikedev/comments/6yieqt/testing_nonsquare_tiles_on_a_square_grid_what_do/)
of [@Sin-tel](https://github.com/Sin-tel)'s dungeon-crawler prototype.

![Gameplay demo](Demos/readme-demo.mp4)
---
## Todo list
- [ ] Tilemaps
	- [x] PNG-based maps
	- [x] Large map support
	- [x] Pseudo-3D tile support
	- [ ] [Autotiling using bitmasking](http://www.angryfishstudios.com/2011/04/adventures-in-bitmasking/)
- [ ] Mobs / player
	- [x] Class-defined mobs
	- [x] HP
	- [ ] Death
	- [ ] Real behaviour
		- [ ] Pathfinding
		- [ ] Combat
- [ ] Graphics
	- [x] Camera follows and stops at edges
	- [ ] Lighting / *line-of-sight*
		- [x] Circle of light
		- [ ] [True LOS](https://www.redblobgames.com/articles/visibility/)

## Installing

The game can be run after [installing Löve](https://love2d.org/wiki/Getting_Started)
