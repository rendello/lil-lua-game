#!/usr/bin/lua5.3

local math = require('math')
local inspect = require('Libraries/inspect')  -- view tables

Timer = require('Libraries/chrono/Timer')     -- timers
Object = require('Libraries/Classic/classic') -- class system

tile = {
	wall = {
		img = love.graphics.newImage('Assets/8/wall.png'),
		passable = false
	},
	wall_II = {
		img = love.graphics.newImage('Assets/8/wall_II.png'),
		passable = false
	},
	floor = {
		img = love.graphics.newImage('Assets/8/floor.png'),
		passable = true
	},
	lava = {
		img = love.graphics.newImage('Assets/8/lava.png'),
		passable = true
	}
}

Entity = Object:extend()

function Entity:new(x, y, image_path)
	self.x = x or 0
	self.y = y or 0
	self.image = love.graphics.newImage(image_path)
end

Mob = Entity:extend()

function Mob:new(x, y, image_path, hp, attack, move_speed)
	Mob.super.new(self, x, y, image_path)
	self.hp = hp or 0
	self.attack = attack or 0
	self.move_speed = move_speed or 0
end


function Mob:move(direction)
	if direction == 'up' or direction == 'down' then
		if direction == 'up' then possMove = self.y - 1 else
			possMove = self.y + 1
		end
		if tiledict[map.tilemap[self.x][possMove]].passable then
			self.y = possMove
		end
	end
	if direction == 'left' or direction == 'right' then
		if direction == 'left' then
			possMove = self.x - 1
		else
			possMove = self.x + 1
		end
		if tiledict[map.tilemap[possMove][self.y]].passable then
			self.x = possMove
		end
	end
end

player = Mob(10, 10, 'Assets/8/player.png', 10, 10, 1)
player['xIndex'] = 0
player['yIndex'] = 0

bat = Mob(16, 10, 'Assets/8/player.png', 100, 10, 1)

entities = {['player']=player, ['bat']=bat}

function generateHeightOffsets(standardHeight)
	-- Tiles have a standard height, but can be taller to give a pseudo 3D
	-- look.  This calculates the pixel offset needed for each tile, since they
	-- are to be lifted from their natural position when drawn
	for _, item in pairs(tile) do
		item['heightOffset'] = item.img:getHeight() - standardHeight
	end
end


tiledict = {
	[0] = tile.floor,
	[1] = tile.wall,
	[2] = tile.lava,
	[3] = tile.wall_II
}


maptile_colors = {
	['1,1,1'] = 0,
	['0,0,0'] = 1,
	['1,0,0'] = 2,
	['0,1,0'] = 3
}


function makeMap(path)
	-- Takes a png and reads it into a 2D table

	local imagedata = love.image.newImageData(path)
	local image = love.graphics.newImage(imagedata)
	local map = {}

	for x=1, image:getWidth() do
		map[x] = {}
		for y=1, image:getHeight() do

			-- Lua starts indexes at 1, images at 0
			r, g, b, _ = imagedata:getPixel(x - 1, y - 1)
			val = maptile_colors[table.concat({r,g,b}, ',')]
			map[x][y] = val
		end
	end
	return map
end


function camera()
	-- These values deal with the camera moving out of bounds. If it's 
	-- about to, viewDistance is counteracted by x/yOffset
	xOffset = 0
	yOffset = 0
	while player.x - viewDistance + xOffset < 1 do xOffset = xOffset + 1 end
	while player.y - viewDistance + yOffset < 1 do yOffset = yOffset + 1 end
	while player.x + viewDistance + xOffset > map.width  do xOffset = xOffset - 1 end
	while player.y + viewDistance + yOffset > map.height do yOffset = yOffset - 1 end

	-- Tiles are drawn in the same window, defined by the player's viewDistance.
	xIndex = 0
	yIndex = 0
	love.graphics.setColor(255, 255, 255)
 	for x = player.x - viewDistance + xOffset, player.x + viewDistance + xOffset do
		xIndex = xIndex + 1
 		for y = player.y - viewDistance + yOffset, player.y + viewDistance + yOffset do
		yIndex = yIndex + 1

			-- placing each visible map tile
			-->  currentTile:    [table] tile being placed
			-->  xIndex, yIndex: position in regards to viewable area
			-->  heightOffset:   see generateHeightOffsets()
			currentTile = tiledict[map.tilemap[x][y]]
 			love.graphics.draw(currentTile.img, xIndex * tile_size, (yIndex * tile_size) - currentTile.heightOffset)

			for entity_id, entity in pairs(entities) do
				if entity.x == x and entity.y == y then
					love.graphics.draw(entity.image, xIndex * tile_size, yIndex * tile_size)

					-- the player's x and y (relative to the screen) are needed
					-- for the light shader, which is outside of this whole loop
					if entity_id == 'player' then
						player.xIndex = xIndex
						player.yIndex = yIndex
					end
				end
				
			end
 		end
		yIndex = 0
 	end
end


function love.load()
	map = {}
	map.tilemap = makeMap('Maps/mapp7.png')
	map.width = #map.tilemap[1]
	map.height = #map.tilemap

	viewDistance = 7
	tile_size = 8
	offset = {x = 0, y = 0}
	scale = 3

	my_shader = love.graphics.newShader('Shaders/palette.glsl')

	timer = Timer()
	timer:every(.5, function() bat:move(move_dir) end)

	light_data = love.image.newImageData(540, 540)
	for x=1, 540, 1 do
		for _, y in pairs(get_radius_intersects(x)) do
			light_data:setPixel(x, y, 1, 1, 1, 1)
		end
	end
	circle_of_light = love.graphics.newImage(light_data)
	mask_shader = love.graphics.newShader('Shaders/mask.glsl')

	generateHeightOffsets(tile_size)
end


function mask_shader_function()
	love.graphics.circle("fill", (player.xIndex * tile_size) + (tile_size / 2), (player.yIndex * tile_size) + (tile_size / 2), 40)

end


function love.draw()
	game_canvas = love.graphics.newCanvas(600, 600)
	game_canvas:setFilter("nearest", "nearest")
	love.graphics.setCanvas{game_canvas, stencil = true}

	love.graphics.stencil(mask_shader_function, "replace", 1)
	love.graphics.setStencilTest("greater", 0)

	camera()

	love.graphics.setStencilTest()
	love.graphics.print(player.hp, 100, 100)

	love.graphics.setCanvas()
	love.graphics.draw(game_canvas, 0, 0, 0, scale, scale)
end


function get_radius_intersects(x)
	circle = {}
	circle.x = 200
	circle.y = 200
	circle.radius = 10
	local intersects = {}
	for y=1, 540, 1 do
		if (x - circle.x)^2 + (y - circle.y)^2 <= circle.radius^2 - 10 then
			intersects[#intersects+1] = y
		end
	end
	return intersects
end


function love.keypressed(key)
	local keymap = {
		['w'] = 'up',
		['a'] = 'left',
		['s'] = 'down',
		['d'] = 'right',
		['h'] = 'left',
		['j'] = 'down',
		['k'] = 'up',
		['l'] = 'right'
	}
	key = keymap[key] or key
	if key == 'up' or key == 'down' or key == 'left' or key == 'right' then
		player:move(key)
	end
end


count = 0
function love.update(dt)
	timer:update(dt)
	if map.tilemap[player.x][player.y] == 2 then
		count = count + dt
		if count > 0.1 then
			player.hp = player.hp - 1
			count = 0
		end
	else
		count = 0
	end

	local move_num = math.random(1, 4)
	local move_map = {
		[1] = 'up',
		[2] = 'left',
		[3] = 'down',
		[4] = 'right'
	}
	move_dir = move_map[move_num]
end


-- graph = {}
-- costs = {}
-- parents = {}
-- 
-- function dijkstra()
-- 	local node = find_cheapest_node(costs)
-- 	while node != nil do
-- 		local cost = costs[node]
-- 		local neighbors = graph[node]
-- 		for n, _ in pairs(neighbors) do
-- 			local new_cost = cost + neighbors[n]
-- 			if costs[n] > new_cost then
-- 				local costs[n] = new_cost
-- 				local parents[n] = node
-- 			end
-- 		end
-- 		table.insert(processed, node)
-- 		local node = find_cheapest_node(costs)
-- 	end
-- end
